/* Copyright (C) Keir Yurkiw and contributers */

#define _POSIX_C_SOURCE 200809L
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"
#include "memory.h"
#include "utils.h"
#include "window.h"

static inline char *
get_shader_type_string(GLenum type)
{
	switch (type) {
		case GL_VERTEX_SHADER: return "vertex";
		case GL_FRAGMENT_SHADER: return "fragment";
		case GL_GEOMETRY_SHADER: return "geometry";
		default: return "unknown";
	}
}

static GLuint
create_shader(char *path, GLenum type)
{
	file shader_file = read_file(path);
	if (shader_file.data == NULL)
		return 0;

	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, (const char * const *)&shader_file.data, NULL);
	glCompileShader(shader);

	destroy_file(&shader_file);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {
		GLint info_log_length;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);

		GLchar *info_log = alloc(info_log_length);
		glGetShaderInfoLog(shader, info_log_length, NULL, info_log);

		if (info_log[info_log_length-2] == '\n')
			info_log[info_log_length-2] = '\0';

		VPERROR("Failed to compile %s shader '%s': %s",
				get_shader_type_string(type), path, info_log);
		FREE(info_log);

		return 0;
	}

	return shader;
}

static GLuint
create_shader_program(GLuint shader_count, ...)
{
	GLuint shader_program = glCreateProgram();

	va_list shaders;
	va_start(shaders, shader_count);

	for (size_t i = 0; i < shader_count; i++)
		glAttachShader(shader_program, va_arg(shaders, unsigned int));

	glLinkProgram(shader_program);

	va_end(shaders);

	va_start(shaders, shader_count);

	for (size_t i = 0; i < shader_count; i++)
		glDetachShader(shader_program, va_arg(shaders, unsigned int));

	va_end(shaders);

	GLint status;
	glGetProgramiv(shader_program, GL_LINK_STATUS, &status);
	if (!status) {
		GLint info_log_length;
		glGetProgramiv(shader_program, GL_INFO_LOG_LENGTH, &info_log_length);

		GLchar *info_log = alloc(info_log_length);
		glGetProgramInfoLog(shader_program, info_log_length, NULL, info_log);
		VPERROR("Failed to link shader program: %s", info_log);

		if (info_log[info_log_length-2] == '\n')
			info_log[info_log_length-2] = '\0';

		FREE(info_log);

		return 0;
	}

	return shader_program;
}

static void
delete_shaders(GLuint shader_count, ...)
{
	va_list shaders;
	va_start(shaders, shader_count);

	for (size_t i = 0; i < shader_count; i++)
		glDeleteShader(va_arg(shaders, unsigned int));

	va_end(shaders);
}

static bool
initialize_glew(void)
{
	GLenum ret = glewInit();
	if (ret != GLEW_OK) {
		VPERROR("Failed to initialize GLEW: %s", glewGetString(ret));
		return false;
	}

	if (!GLEW_VERSION_3_3) {
		VPERROR("OpenGL version %d.%d is required", OGL_MAJOR, OGL_MINOR);
		return false;
	}

	return true;
}

int
main(void)
{
	if (!initialize_glfw())
		return EXIT_FAILURE;

	GLFWwindow *window = create_window();
	if (!window) {
		cleanup_glfw(NULL);
		return EXIT_FAILURE;
	}

	if (!initialize_glew()) {
		cleanup_glfw(window);
		return EXIT_FAILURE;
	}

	GLuint vert_shader = create_shader("shaders/shader.vert.glsl", GL_VERTEX_SHADER);
	GLuint frag_shader = create_shader("shaders/shader.frag.glsl", GL_FRAGMENT_SHADER);
	if (!vert_shader || !frag_shader)
		return EXIT_FAILURE;

	GLuint shader_program = create_shader_program(2, vert_shader, frag_shader);
	if (!shader_program) {
		cleanup_glfw(window);
		return EXIT_FAILURE;
	}

	delete_shaders(2, vert_shader, frag_shader);

	float vertices[] = {
		 0.0f,  0.5f, 0.0f, 1.0f, /* top */
		 0.5f, -0.5f, 0.0f, 1.0f, /* right */
		-0.5f, -0.5f, 0.0f, 1.0f, /* left */

		1.0f, 0.0f, 0.0f, 1.0f, /* red */
		0.0f, 1.0f, 0.0f, 1.0f, /* green */
		0.0f, 0.0f, 1.0f, 1.0f, /* blue */
	};
	size_t stride = 4 * sizeof(float);

	GLuint vbo, vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, stride, (void *)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, stride, (void *)(sizeof(vertices)/2));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glfwShowWindow(window);
	run_window_event_loop(window, shader_program, vao);

	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteProgram(shader_program);
	cleanup_glfw(window);
}
