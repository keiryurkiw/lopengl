# Copyright (C) Keir Yurkiw and contributers

SRC = utils.c \
      window.c \
      opengl.c

HEADERS = utils.h \
          error.h \
          memory.h \
          window.h

OBJ = $(SRC:.c=.o)
