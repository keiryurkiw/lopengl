/* Copyright (C) 2022 Keir Yurkiw and contributers */

#define _POSIX_C_SOURCE 200809L

/* just assume everything that isn't windows is unix */
#ifdef _WIN32
# define WINDOWS
#else
# define UNIX
#endif

#ifdef UNIX
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#endif

#ifdef WINDOWS
#include <windows.h>
#endif

#include "error.h"
#include "memory.h"
#include "utils.h"

#ifdef UNIX

#define CLOSE(fd) do { close(fd); fd = -1; } while (0)

file
read_file(char *path)
{
	file file = { .data = NULL };

	int fd = open(path, O_RDONLY);
	if (fd == -1) {
		VPERRNUM("Failed to open file '%s'", path);
		return file;
	}

	struct stat sb;
	file.size = (fstat(fd, &sb) != 0) ? 0 : sb.st_size;
	if (!file.size) {
		VPERRNUM("Failed to get size of file '%s'", path);
		CLOSE(fd);
		return file;
	}

	file.data = mmap(NULL, file.size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (file.data == MAP_FAILED) {
		VPERRNUM("Failed to map file '%s'", path);
		ZERO(&file);
		CLOSE(fd);
		return file;
	}

	CLOSE(fd);

	return file;
}

void
destroy_file(file *file)
{
	munmap(file->data, file->size);
	ZERO(file);
}

#endif /* UNIX */

#ifdef WINDOWS

#define VPWIN32_ERROR(fmt, ...) do { \
	char *buffer; \
	FormatMessage( \
			FORMAT_MESSAGE_ALLOCATE_BUFFER | \
			FORMAT_MESSAGE_FROM_SYSTEM | \
			FORMAT_MESSAGE_IGNORE_INSERTS, \
			NULL, GetLastError(), \
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), \
			(LPTSTR)&buffer, 0, NULL); \
\
	size_t buf_len = strlen(buffer); \
	if (buffer[buf_len-1] == '\n') \
		buffer[buf_len-1] = '\0'; \
\
	VPERROR(fmt ": %s", __VA_ARGS__, buffer); \
\
	LocalFree(buffer); \
	buffer = NULL; } while (0)

#define CLOSE_HANDLE(hdl) do { \
	CloseHandle(hdl); \
	hdl = INVALID_HANDLE_VALUE; } while (0)

#define DWORD_HI(x) 0
#define DWORD_LO(x) (x)

file
read_file(char *path)
{
	file file = {
		.data = NULL,
		.handle = INVALID_HANDLE_VALUE,
		.map = INVALID_HANDLE_VALUE,
	};

	file.handle = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, NULL,
			OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (file.handle == INVALID_HANDLE_VALUE) {
		VPWIN32_ERROR("Failed to open file '%s'", path);
		return file;
	}

	DWORD size = GetFileSize(file.handle, NULL);
	if (size == INVALID_FILE_SIZE) {
		VPWIN32_ERROR("Failed to get size of file '%s'", path);
		CLOSE_HANDLE(file.handle);
		return file;
	} else {
		file.size = size;
	}

	file.map = CreateFileMapping(file.handle, NULL, PAGE_READONLY,
			DWORD_HI(file.size), DWORD_LO(file.size), NULL);
	if (file.map == INVALID_HANDLE_VALUE) {
		VPWIN32_ERROR("Failed to map file '%s'", path);
		CLOSE_HANDLE(file.handle);
		file.size = 0;
		return file;
	}

	file.data = MapViewOfFile(file.map, FILE_MAP_READ, 0, 0, file.size);
	if (!file.data) {
		VPWIN32_ERROR("Failed to map view of file '%s'", path);
		CLOSE_HANDLE(file.map);
		CLOSE_HANDLE(file.handle);
		file.size = 0;
		return file;
	}

	return file;
}

void
destroy_file(file *file)
{
	UnmapViewOfFile(file->data);
	file->data = NULL;
	file->size = 0;

	CLOSE_HANDLE(file->map);
	CLOSE_HANDLE(file->handle);
}

#endif /* WINDOWS */
