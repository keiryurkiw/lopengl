# Copyright (C) Keir Yurkiw and contributers

# Makefile header to be used only for development

CC = gcc
CFLAGS = -O0 -g -pipe -march=native -Wall -Wextra -pedantic -pedantic-errors -std=c99 \
         -Wmissing-prototypes
#CFLAGS += -fanalyzer
CPPFLAGS =
