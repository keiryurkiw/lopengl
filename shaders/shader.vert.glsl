#version 330 core

layout(location = 0) in vec4 pos;
layout(location = 1) in vec4 colour;

out vec4 tri_colour;

void main()
{
	gl_Position = pos;
	tri_colour = colour;
}
