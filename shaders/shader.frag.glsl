#version 330 core

out vec4 frag_colour;

in vec4 tri_colour;

void main()
{
	frag_colour = tri_colour;
}
