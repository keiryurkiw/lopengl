/* Copyright (C) 2022 Keir Yurkiw and contributers */

#ifndef MEMORY_H
#define MEMORY_H

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"

#define FREE(ptr) do { \
	ASSERT(ptr != NULL, "Double free detected!"); \
	free(ptr); ptr = NULL; } while (0)

#define REALLOC(ptr, size) do { \
	ASSERT(size > 0, "Tried to reallocate 0 bytes!"); \
	ptr = realloc(ptr, size); \
	if (ptr == NULL) { \
		VPERRNUM("Failed to reallocate %zu bytes", size); \
		exit(errno); \
	} } while (0)

#define ARR_ZERO(nelems, ptr) do { \
	ASSERT(nelems > 0, "Tried to zero 0 elements!"); \
	ASSERT(ptr != NULL, "Tried to zero a NULL pointer!"); \
	memset(ptr, 0, sizeof(*ptr) * (nelems)); } while (0)
#define ZERO(ptr) ARR_ZERO(1, ptr)

static inline void *
alloc(size_t size)
{
	ASSERT(size > 0, "Tried to allocate 0 bytes!");

	void *ptr = malloc(size);
	if (ptr == NULL) {
		VPERRNUM("Failed to allocate %zu bytes", size);
		exit(errno);
	}

	return ptr;
}

static inline void *
arr_alloc(int nelems, size_t size)
{
	ASSERT(nelems > 0, "Tried to allocate 0 elements!");
	ASSERT(size > 0, "Tried to allocate 0 bytes!");

	return alloc(nelems * size);
}

#endif /* MEMORY_H */
