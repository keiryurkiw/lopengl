/* Copyright (C) Keir Yurkiw and contributers */

#define _POSIX_C_SOURCE 200809L
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>

#include "error.h"
#include "utils.h"
#include "window.h"

static inline char *
get_glfw_error(int code)
{
	switch (code) {
		case GLFW_NOT_INITIALIZED: return "GLFW_NOT_INITIALIZED";
		case GLFW_NO_CURRENT_CONTEXT: return "GLFW_NO_CURRENT_CONTEXT";
		case GLFW_INVALID_ENUM: return "GLFW_INVALID_ENUM";
		case GLFW_INVALID_VALUE: return "GLFW_INVALID_VALUE";
		case GLFW_OUT_OF_MEMORY: return "GLFW_OUT_OF_MEMORY";
		case GLFW_API_UNAVAILABLE: return "GLFW_API_UNAVAILABLE";
		case GLFW_VERSION_UNAVAILABLE: return "GLFW_VERSION_UNAVAILABLE";
		case GLFW_PLATFORM_ERROR: return "GLFW_PLATFORM_ERROR";
		case GLFW_FORMAT_UNAVAILABLE: return "GLFW_FORMAT_UNAVAILABLE";
		case GLFW_NO_WINDOW_CONTEXT: return "GLFW_NO_WINDOW_CONTEXT";
		default: return "unknown error";
	}
}

static void
glfw_error_callback(int code, const char *description)
{
	VPERROR("glfw: %s: %s", get_glfw_error(code), description);
}

bool
initialize_glfw(void)
{
	if (!glfwInit()) {
		PERROR("GLFW initialization failed");
		return false;
	}

	glfwSetErrorCallback(glfw_error_callback);

	return true;
}

GLFWwindow *
create_window(void)
{
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OGL_MAJOR);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OGL_MINOR);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "OpenGL", NULL, NULL);
	if (!window) {
		PERROR("Failed to create window");
		glfwTerminate();
		return window;
	}

	glfwMakeContextCurrent(window);
	glViewport(0, 0, WIDTH, HEIGHT);

	return window;
}

static void
process_input(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

static void
draw(GLuint program, GLuint vao)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(program);

	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);
	glUseProgram(0);
}

void
run_window_event_loop(GLFWwindow *window, GLuint program, GLuint vao)
{
	enum frame_cap { NO_LIMIT, FPS_60, FPS_30 };
	glfwSwapInterval(FPS_60);

	float delta_time, start, end;
	while (!glfwWindowShouldClose(window)) {
		start = glfwGetTime();

		process_input(window);

		draw(program, vao);

		glfwSwapBuffers(window);
		glfwPollEvents();

		end = glfwGetTime();

		delta_time = end - start;
		printf("\033[1A\033[K" "\033[1A\033[K" "Delta time: %f\nFPS: %u\n",
				delta_time, (unsigned int)round(1.0f / delta_time));
	}
}

void
cleanup_glfw(GLFWwindow *window)
{
	if (window)
		glfwDestroyWindow(window);
	glfwTerminate();
}
