/* Copyright (C) 2022 Keir Yurkiw and contributers */

#ifndef ERROR_H
#define ERROR_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ANSI_RED(msg) "\x1b[01;31m" msg "\x1b[m"

#define VPERROR(fmt, ...) \
	fprintf(stderr, "opengl: " ANSI_RED("error: ") fmt "\n", __VA_ARGS__)
#define PERROR(msg) fputs("opengl: " ANSI_RED("error: ") msg "\n", stderr)

#define PERRNUM(msg) VPERROR(msg ": %s", strerror(errno))
#define VPERRNUM(fmt, ...) VPERROR(fmt ": %s", __VA_ARGS__, strerror(errno))

#ifdef NDEBUG
#define ASSERT(expr, msg) ((void)0)
#else
#define ASSERT(expr, msg) if (!(expr)) { \
	fprintf(stderr, "opengl: " ANSI_RED("assert: ") \
			"[%s:%d] from %s(): Assertion '%s' FAILED: %s\n", \
			__FILE__, __LINE__, __func__, #expr, msg); \
	abort(); }
#endif /* NDEBUG */

#endif /* UTILS_H */
