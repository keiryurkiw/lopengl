# Copyright (C) 2022 Keir Yurkiw and contributers

.POSIX:
.SUFFIXES:

# DEVELOPMENT ONLY
#include devel.mk

PREFIX = /usr/local
PKG_CONFIG = pkg-config
#CC = gcc
#CFLAGS = -O2 -pipe
#CPPFLAGS = -DNDEBUG


PKGS = glfw3 glew
GLCPPFLAGS = `$(PKG_CONFIG) --cflags $(PKGS)` -D_POSIX_C_SOURCE=200809L\
             $(CPPFLAGS)
LDLIBS = `$(PKG_CONFIG) --libs $(PKGS)` -lm

include src.mk

all: opengl

$(OBJ): Makefile
$(OBJ): $(HEADERS)

.SUFFIXES: .c .o
.c.o:
	$(CC) $(GLCPPFLAGS) $(CFLAGS) -c $< -o $@

opengl: $(OBJ)
	$(CC) $(LDFLAGS) $(LDLIBS) $(OBJ) -o $@

clean:
	rm -f opengl $(OBJ)

install: all
	mkdir -p '$(DESTDIR)$(PREFIX)/bin'
	cp -f opengl '$(DESTDIR)$(PREFIX)/bin'
	chmod 755 '$(DESTDIR)$(PREFIX)/bin/opengl'

uninstall:
	rm -f '$(DESTDIR)$(PREFIX)/bin/opengl'

.PHONY: all clean install uninstall
