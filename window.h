/* Copyright (C) 2022 Keir Yurkiw and contributers */

#ifndef WINDOW_H
#define WINDOW_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdbool.h>

bool initialize_glfw(void);
GLFWwindow *create_window(void);
void run_window_event_loop(GLFWwindow *window, GLuint program, GLuint vao);
void cleanup_glfw(GLFWwindow *window);

#endif /* WINDOW_H */
