/* Copyright (C) 2022 Keir Yurkiw and contributers */

#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>
#include <stdio.h>

#ifdef _WIN32
#include <windows.h>
#endif

#define OVERWRITE_LINE(msg) puts("\033[1A" "\033[K" msg)
#define VOVERWRITE_LINE(fmt, ...) printf("\033[1A" "\033[K" fmt "\n", __VA_ARGS__)

enum ogl_version {
	OGL_MAJOR = 3,
	OGL_MINOR = 3,
};

enum window_dimensions {
	WIDTH = 800,
	HEIGHT = 600,
};

typedef struct file_type {
	char *data;
	size_t size;

#ifdef _WIN32
	HANDLE handle;
	HANDLE map;
#endif
} file;

file read_file(char *path);
void destroy_file(file *file);

#endif /* UTILS_H */
